use core::str::FromStr;

use chrono::offset::Utc;
use redis::{from_redis_value, AsyncCommands, FromRedisValue, RedisResult, Value};

#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct Taboo {
    pub guild_id: serenity::model::id::GuildId,
    pub phrase: String,
    pub last_seen: Option<chrono::DateTime<Utc>>,
    pub count: usize,
    pub longest_streak: Option<chrono::Duration>,
    pub alternative_phrases: Option<Vec<String>>,
}

impl FromRedisValue for Taboo {
    #[tracing::instrument(level = "trace")]
    fn from_redis_value(v: &Value) -> RedisResult<Self> {
        let v: std::collections::HashMap<String, String> = from_redis_value(v)?;
        let t = Taboo {
            guild_id: serenity::model::id::GuildId::from(
                v["guild_id"]
                    .parse::<u64>()
                    .expect("Error getting guild_id"),
            ),
            phrase: v["phrase"].to_owned(),
            last_seen: v
                .get("last_seen")
                .map(|x| chrono::DateTime::<Utc>::from_str(x).unwrap()),
            count: v["count"].parse::<usize>().expect("Error getting count"),
            longest_streak: v
                .get("longest_streak")
                .map(|x| chrono::Duration::seconds(x.parse::<i64>().unwrap())),
            alternative_phrases: v
                .get("alternative_phrases")
                .map(|x| serde_json::from_str(x).expect("Error deserializing taboo")),
        };
        Ok(t)
    }
}

impl Taboo {
    #[tracing::instrument(level = "trace", skip(redis))]
    pub async fn increment_count(&mut self, redis: &mut redis::aio::ConnectionManager) {
        let _: () = redis
            .hincr(self.guild_id.to_string() + ":" + &self.phrase, "count", 1)
            .await
            .expect("Error adding count for taboo");
        self.count += 1;
    }

    #[tracing::instrument(level = "trace", skip(redis))]
    pub async fn update_last_seen(
        &mut self,
        redis: &mut redis::aio::ConnectionManager,
        value: &str,
    ) {
        let _: () = redis
            .hset(
                self.guild_id.to_string() + ":" + &self.phrase,
                "last_seen",
                value,
            )
            .await
            .expect("Error updating last_seen for taboo");
        self.last_seen = chrono::DateTime::<Utc>::from_str(value).ok();
    }

    #[tracing::instrument(level = "trace", skip(redis))]
    pub async fn update_longest_streak(
        &mut self,
        redis: &mut redis::aio::ConnectionManager,
        seconds: i64,
    ) {
        let _: () = redis
            .hset(
                self.guild_id.to_string() + ":" + &self.phrase,
                "longest_streak",
                seconds,
            )
            .await
            .expect("Error updating longest_streak for taboo");
        self.longest_streak = Some(chrono::Duration::seconds(seconds));
    }
}
