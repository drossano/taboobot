use std::collections::BTreeMap;
use std::convert::TryInto;

use chrono::offset::Utc;
use regex::RegexSetBuilder;
use serenity::{
    async_trait,
    model::{
        channel::Message,
        event::MessageUpdateEvent,
        gateway::Ready,
        interactions::{
            application_command::{
                ApplicationCommand, ApplicationCommandInteractionDataOptionValue,
                ApplicationCommandOptionType,
            },
            Interaction, InteractionApplicationCommandCallbackDataFlags, InteractionResponseType,
        },
        misc::Mention,
    },
    prelude::*,
};
use strip_markdown::*;
use tracing::{debug, debug_span, error, info, trace};

use crate::guild::Guild;
use crate::redis_connection::RedisConnectionManager;
use crate::taboo::Taboo;

const ADMINISTRATOR_COMMANDS: [&str; 2] = ["add", "delete"];

pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
    #[tracing::instrument(level = "trace", skip(self, ctx))]
    async fn message(&self, ctx: Context, message: Message) {
        // Since the bot specifies which taboo was broken in its message
        // this prevents recursion by ignoring messages from the bot itself
        if ctx.cache.current_user_id().await == message.author.id {
            trace!("Message is from the bot itself");
            return;
        }

        // Sentry data
        sentry::configure_scope(|scope| {
            scope.set_user(Some(sentry::User {
                id: Some(message.author.id.to_string()),
                username: Some(format!(
                    "{}#{}",
                    &message.author.name,
                    message.author.discriminator.to_string()
                )),
                ..Default::default()
            }));

            let mut context = BTreeMap::new();
            context.insert(
                String::from("guild"),
                message.guild_id.unwrap_or_default().to_string().into(),
            );
            context.insert(String::from("content"), message.content.clone().into());
            scope.set_context("message", sentry::protocol::Context::Other(context));
        });

        if let Some(guild_id) = message.guild_id {
            let mut ctx_data = ctx.data.write().await;
            let redis = ctx_data.get_mut::<RedisConnectionManager>().unwrap();

            let guild = Guild::new(redis, guild_id).await;

            debug!(?guild.taboos);

            let mut patterns: Vec<(&Taboo, String)> = Vec::new();
            for taboo in &guild.taboos {
                let _span = debug_span!("build_pattern", ?taboo).entered();
                let pattern = format!("{}{}{}", r"\b", &taboo.phrase, r"\b");
                patterns.push((taboo, pattern));
                if let Some(alt_phrases) = &taboo.alternative_phrases {
                    for alt_phrase in alt_phrases {
                        let pattern = format!("{}{}{}", r"\b", alt_phrase, r"\b");
                        patterns.push((taboo, pattern));
                    }
                }
            }

            debug!(?patterns);

            let matches: Vec<usize> = RegexSetBuilder::new(patterns.iter().map(|p| &p.1))
                .case_insensitive(true)
                .build()
                .unwrap()
                .matches(&strip_markdown(&message.content))
                .iter()
                .collect();

            debug!(?matches);

            let found: Vec<&Taboo> = matches.iter().map(|&m| patterns[m].0).collect();

            debug!(?found);

            for taboo in found {
                debug!(?taboo);
                let mut taboo = taboo.to_owned();
                taboo.increment_count(redis).await;
                match taboo.last_seen {
                    Some(last_seen) => {
                        let difference = message.timestamp.signed_duration_since::<Utc>(last_seen);
                        let mut f_difference = timeago::Formatter::new();
                        f_difference.num_items(3);
                        f_difference.too_low("just now");
                        let mut f_streak = timeago::Formatter::new();
                        f_streak.num_items(3);
                        f_streak.ago("");

                        let response_content: String;

                        match taboo.longest_streak {
                            Some(longest_streak) => {
                                if difference > longest_streak {
                                    // This is the new longest_streak
                                    response_content = format!("You have broken the taboo placed on \"{}\". This taboo has been broken {} times.\nThe last time this taboo was broken was {}.\nThis is your new record! Your previous best was {}.",
                                        taboo.phrase,
                                        taboo.count,
                                        f_difference.convert(difference.to_std().expect("Error converting time::Duration to std::time::Duration. This should not be possible.")),
                                        f_streak.convert(longest_streak.to_std().expect("Error converting time::Duration to std::time::Duration. This should not be possible."))
                                    );
                                    taboo
                                        .update_longest_streak(redis, difference.num_seconds())
                                        .await;
                                } else {
                                    // This is not the new longest_streak
                                    response_content = format!("You have broken the taboo placed on \"{}\". This taboo has been broken {} times.\nThe last time this taboo was broken was {}.\nYour longest streak is {}.",
                                        taboo.phrase,
                                        taboo.count,
                                        f_difference.convert(difference.to_std().expect("Error converting time::Duration to std::time::Duration. This should not be possible.")),
                                        f_streak.convert(longest_streak.to_std().expect("Error converting time::Duration to std::time::Duration. This should not be possible."))
                                    );
                                }
                            }
                            None => {
                                response_content = format!("You have broken the taboo placed on \"{}\". This taboo has been broken {} times.\nThe last time this taboo was broken was {}.",
                                    taboo.phrase,
                                    taboo.count,
                                    f_difference.convert(difference.to_std().expect("Error converting time::Duration to std::time::Duration. This should not be possible."))
                                );
                                taboo
                                    .update_longest_streak(redis, difference.num_seconds())
                                    .await;
                            }
                        }

                        if let Err(why) = message.reply(&ctx.http, response_content).await {
                            sentry::capture_error(&why);
                            error!("Couldn't reply: {}", why);
                        }
                    }
                    None => {
                        // This is the first time taboo has been seen
                        let response_content = format!("You have broken the taboo placed on \"{}\". This is the first time this taboo has been broken.",
                            taboo.phrase
                        );
                        if let Err(why) = message.reply(&ctx.http, response_content).await {
                            sentry::capture_error(&why);
                            error!("Couldn't reply: {}", why);
                        }
                    }
                }
                taboo
                    .update_last_seen(redis, &format!("{}", message.timestamp))
                    .await;
            }
        }
    }

    #[tracing::instrument(level = "trace", skip(self, ctx))]
    async fn message_update(
        &self,
        ctx: Context,
        _old_message: Option<Message>,
        _message: Option<Message>,
        event: MessageUpdateEvent,
    ) {
        // Rich embedded content seems to be created as a message_update event
        // with no author. The event doesn't seem to indicate that it was
        // to create an embed, so this simply skips the event if there's no author
        if event.clone().author.is_none() {
            trace!("Message update is (probably) from Discord itself");
            return;
        }

        let author = event
            .clone()
            .author
            .expect("Error getting an author in message_update.");
        let content = event
            .clone()
            .content
            .expect("Error getting content in message_update.");
        let timestamp = event
            .edited_timestamp
            .expect("Error getting edited_timestamp in message_update.");

        // Since the bot specifies which taboo was broken in its message
        // this prevents recursion by ignoring message_updates from the bot itself
        if ctx.cache.current_user_id().await == author.id {
            trace!("Message update is from the bot itself");
            return;
        }

        // Sentry data
        sentry::configure_scope(|scope| {
            scope.set_user(Some(sentry::User {
                id: Some(author.id.to_string()),
                username: Some(author.tag()),
                ..Default::default()
            }));

            let mut context = BTreeMap::new();
            context.insert(
                String::from("guild"),
                event.guild_id.unwrap_or_default().to_string().into(),
            );
            context.insert(String::from("content"), content.clone().into());
            scope.set_context("message_update", sentry::protocol::Context::Other(context));
        });

        if let Some(guild_id) = event.guild_id {
            let mut ctx_data = ctx.data.write().await;
            let redis = ctx_data.get_mut::<RedisConnectionManager>().unwrap();

            let guild = Guild::new(redis, guild_id).await;

            debug!(?guild.taboos);

            let mut patterns: Vec<(&Taboo, String)> = Vec::new();
            for taboo in &guild.taboos {
                let _span = debug_span!("build_pattern", ?taboo).entered();
                let pattern = format!("{}{}{}", r"\b", &taboo.phrase, r"\b");
                patterns.push((taboo, pattern));
                if let Some(alt_phrases) = &taboo.alternative_phrases {
                    for alt_phrase in alt_phrases {
                        let pattern = format!("{}{}{}", r"\b", alt_phrase, r"\b");
                        patterns.push((taboo, pattern));
                    }
                }
            }

            debug!(?patterns);

            let matches: Vec<usize> = RegexSetBuilder::new(patterns.iter().map(|p| &p.1))
                .case_insensitive(true)
                .build()
                .unwrap()
                .matches(&strip_markdown(&content))
                .iter()
                .collect();

            debug!(?matches);

            let found: Vec<&Taboo> = matches.iter().map(|&m| patterns[m].0).collect();

            debug!(?found);

            for taboo in found {
                debug!(?taboo);
                let mut taboo = taboo.to_owned();
                taboo.increment_count(redis).await;
                match taboo.last_seen {
                    Some(last_seen) => {
                        let difference = timestamp.signed_duration_since::<Utc>(last_seen);
                        let mut f_difference = timeago::Formatter::new();
                        f_difference.num_items(3);
                        f_difference.too_low("just now");
                        let mut f_streak = timeago::Formatter::new();
                        f_streak.num_items(3);
                        f_streak.ago("");

                        let response_content: String;

                        match taboo.longest_streak {
                            Some(longest_streak) => {
                                if difference > longest_streak {
                                    // This is the new longest_streak
                                    response_content = format!("<@{}> has edited history and broken the current taboo placed on \"{}\". This taboo has been broken {} times.\nThe last time this taboo was broken was {}.\nThis is your new record! Your previous best was {}.\n\nEvidence: {}",
                                        author.id,
                                        taboo.phrase,
                                        taboo.count,
                                        f_difference.convert(difference.to_std().expect("Error converting time::Duration to std::time::Duration. This should not be possible.")),
                                        f_streak.convert(longest_streak.to_std().expect("Error converting time::Duration to std::time::Duration. This should not be possible.")),
                                        event.id.link_ensured(&ctx.http, event.channel_id, None).await
                                    );
                                    taboo
                                        .update_longest_streak(redis, difference.num_seconds())
                                        .await;
                                } else {
                                    // This is not the new longest_streak
                                    response_content = format!("<@{}> has edited history and broken the current taboo placed on \"{}\". This taboo has been broken {} times.\nThe last time this taboo was broken was {}.\nYour longest streak is {}.\n\nEvidence: {}",
                                        author.id,
                                        taboo.phrase,
                                        taboo.count,
                                        f_difference.convert(difference.to_std().expect("Error converting time::Duration to std::time::Duration. This should not be possible.")),
                                        f_streak.convert(longest_streak.to_std().expect("Error converting time::Duration to std::time::Duration. This should not be possible.")),
                                        event.id.link_ensured(&ctx.http, event.channel_id, None).await
                                    );
                                }
                            }
                            None => {
                                response_content = format!("<@{}> has edited history and broken the current taboo placed on \"{}\". This taboo has been broken {} times.\nThe last time this taboo was broken was {}.\n\nEvidence: {}",
                                    author.id,
                                    taboo.phrase,
                                    taboo.count,
                                    f_difference.convert(difference.to_std().expect("Error converting time::Duration to std::time::Duration. This should not be possible.")),
                                    event.id.link_ensured(&ctx.http, event.channel_id, None).await
                                );
                                taboo
                                    .update_longest_streak(redis, difference.num_seconds())
                                    .await;
                            }
                        }

                        if let Err(why) = event
                            .channel_id
                            .send_message(&ctx.http, |message| message.content(response_content))
                            .await
                        {
                            sentry::capture_error(&why);
                            error!("Couldn't respond to message_update: {}", why);
                        }
                    }
                    None => {
                        // This is the first time taboo has been seen
                        let response_content = format!("<@{}> has edited history and broken the current taboo placed on \"{}\". This is the first time this taboo has been broken.\nEvidence: {}",
                            author.id,
                            taboo.phrase,
                            event.id.link_ensured(&ctx.http, event.channel_id, None).await
                        );
                        if let Err(why) = event
                            .channel_id
                            .send_message(&ctx.http, |message| message.content(response_content))
                            .await
                        {
                            sentry::capture_error(&why);
                            error!("Couldn't respond to message_update: {}", why);
                        }
                    }
                }
                taboo
                    .update_last_seen(redis, &format!("{}", timestamp))
                    .await;
            }
        }
    }

    #[tracing::instrument(level = "trace", skip(self, ctx))]
    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        match interaction {
            Interaction::ApplicationCommand(interaction) => {
                // Sentry data
                sentry::configure_scope(|scope| {
                    scope.set_user(Some(sentry::User {
                        id: Some(interaction.user.id.to_string()),
                        username: Some(format!(
                            "{}#{}",
                            &interaction.user.name,
                            interaction.user.discriminator.to_string()
                        )),
                        ..Default::default()
                    }));

                    let mut context = BTreeMap::new();
                    context.insert(
                        String::from("guild"),
                        interaction.guild_id.unwrap_or_default().to_string().into(),
                    );
                    context.insert(String::from("kind"), interaction.kind.num().into());
                    context.insert(String::from("name"), interaction.data.name.clone().into());
                    scope.set_context(
                        "ApplicationCommand",
                        sentry::protocol::Context::Other(context),
                    );
                });

                // Throw away any interactions not sent within a guild
                if let Some(guild_id) = interaction.guild_id {
                    let mut ctx_data = ctx.data.write().await;
                    let redis = ctx_data.get_mut::<RedisConnectionManager>().unwrap();

                    let mut guild = Guild::new(redis, guild_id).await;

                    match interaction.data.name.as_str() {
                        "config" => {
                            // The API calls can require ratelimiting
                            // Send deferred message so the interaction does not fail
                            if let Err(why) = interaction
                                .create_interaction_response(&ctx.http, |response| {
                                    response.kind(InteractionResponseType::DeferredChannelMessageWithSource)
                                })
                                .await
                            {
                                error!("Couldn't respond to config: {}", why);
                            }
                            // Discord's permissions are really dumb
                            // Commands can only be limited to specific User or Role but these limits
                            // can't be set by a server owner, so a command needs to do so.
                            // Until Discord gets it together, this command will have to be manually
                            // limited to a specific Permission.
                            if let Some(member) = &interaction.member {
                                if let Some(permissions) = member.permissions {
                                    if permissions.manage_guild() {
                                        match ApplicationCommand::get_global_application_commands(&ctx.http).await {
                                            Ok(commands) => {
                                                let administrator_role = interaction.data.resolved.roles.keys().last().expect("Expected RoleID");
                                                for command in commands {
                                                    if ADMINISTRATOR_COMMANDS.contains(&command.name.as_str()) {
                                                        if let Err(why) = guild.guild_id.create_application_command_permission(
                                                            &ctx.http, command.id, |data| {
                                                                data.create_permission(|permission| {
                                                                    permission
                                                                        .id(*administrator_role.as_u64())
                                                                        .kind(serenity::model::interactions::application_command::ApplicationCommandPermissionType::Role)
                                                                        .permission(true)
                                                                })
                                                            })
                                                            .await
                                                        {
                                                            sentry::capture_error(&why);
                                                            error!("Couldn't set command permission: {}", why);
                                                        }
                                                    }
                                                }

                                                if let Err(why) = interaction.edit_original_interaction_response(&ctx.http, |message| {
                                                        message.content(format!("The administrator role is now \"{}\".", Mention::from(*administrator_role)))
                                                    })
                                                    .await
                                                {
                                                    sentry::capture_error(&why);
                                                    error!("Couldn't update response to config: {}", why);
                                                }
                                            }
                                            Err(why) => {
                                                error!("Couldn't get application commands: {}", why);
                                                if let Err(why) = interaction.edit_original_interaction_response(&ctx.http, |message| {
                                                        message.content("Something went wrong, most likely with Discord. Try again later.")
                                                    })
                                                    .await
                                                {
                                                    sentry::capture_error(&why);
                                                    error!("Couldn't respond to config: {}", why);
                                                }
                                            }
                                        }
                                    } else if let Err(why) = interaction.edit_original_interaction_response(&ctx.http, |message| {
                                            message.content("You need the `MANAGE_GUILD` permission to perform configuration.")
                                        })
                                        .await
                                    {
                                        sentry::capture_error(&why);
                                        error!("Couldn't respond to config: {}", why);
                                    }
                                }
                            }
                        }

                        // Administrator commands
                        "add" => {
                            if let ApplicationCommandInteractionDataOptionValue::String(
                                taboo,
                            ) =
                                interaction
                                    .data
                                    .options
                                    .get(0)
                                    .unwrap()
                                    .resolved
                                    .as_ref()
                                    .expect("Expected string option")
                            {
                                let mut alt_phrases = Vec::new();
                                for alt_phrase in &interaction.data.options[1..] {
                                    if let ApplicationCommandInteractionDataOptionValue::String(phrase) = alt_phrase.resolved.as_ref().expect("Expected string option") {
                                        alt_phrases.push(phrase.to_string());
                                    }
                                }

                                if !alt_phrases.is_empty() {
                                    guild.set_taboo(redis, taboo, Some(alt_phrases)).await;
                                } else {
                                    guild.set_taboo(redis, taboo, None).await;
                                }

                                if let Err(why) = interaction
                                    .create_interaction_response(&ctx.http, |response| {
                                        response.kind(InteractionResponseType::ChannelMessageWithSource).interaction_response_data(|data| {
                                            data.content(format!("You have placed a taboo on \"{}\".", taboo))
                                        })
                                    })
                                    .await
                                {
                                    sentry::capture_error(&why);
                                    error!("Couldn't respond to taboo creation: {}", why);
                                }
                            }
                        }

                        "delete" => {
                            if !guild.taboos.is_empty() {
                                if let Err(why) = interaction
                                    .create_interaction_response(&ctx.http, |response| {
                                        response.kind(InteractionResponseType::ChannelMessageWithSource).interaction_response_data(|data| {
                                            data
                                            .content("Which taboos do you wish to remove?")
                                            .components(|components| {
                                                components.create_action_row(|row| {
                                                    row.create_select_menu(|menu| {
                                                        menu.custom_id("delete_taboos")
                                                            .placeholder("Current taboos")
                                                            .min_values(1)
                                                            .max_values(guild.taboos.len().try_into().unwrap_or(u64::MAX))
                                                            .options(|mut options| {
                                                                for taboo in guild.taboos {
                                                                    options = options
                                                                        .create_option(
                                                                            |option| {
                                                                                option
                                                                            .label(
                                                                                &taboo.phrase,
                                                                            )
                                                                            .value(
                                                                                &taboo.phrase,
                                                                            )
                                                                            },
                                                                        )
                                                                }
                                                                options
                                                            })
                                                    })
                                                })
                                            })
                                            .flags(InteractionApplicationCommandCallbackDataFlags::EPHEMERAL)
                                        })
                                    })
                                    .await
                                {
                                    sentry::capture_error(&why);
                                    error!("Couldn't respond to taboo deletion: {}", why);
                                }
                            } else if let Err(why) = interaction.create_interaction_response(&ctx.http, |response| {
                                    response
                                        .kind(InteractionResponseType::ChannelMessageWithSource)
                                        .interaction_response_data(|data| {
                                            data.content(
                                                "There are currently no taboos in place.",
                                            )
                                            .flags(InteractionApplicationCommandCallbackDataFlags::EPHEMERAL)
                                        })
                                })
                                .await
                            {
                                sentry::capture_error(&why);
                                error!("Couldn't respond to taboo deletion: {}", why);
                            }
                        }

                        // General commands
                        "list" => {
                            guild.taboos.sort_by(|a, b| a.phrase.cmp(&b.phrase));

                            // TODO: Make this prettier. Embed?
                            for option in &interaction.data.options {
                                match option.name.as_ref() {
                                    "sort" => if let Some(ApplicationCommandInteractionDataOptionValue::String(sort)) = option.resolved.as_ref() {
                                        match sort.as_ref() {
                                            "count" => guild.taboos.sort_by(|a, b| b.count.cmp(&a.count)),
                                            "phrase" => guild.taboos.sort_by(|a, b| a.phrase.cmp(&b.phrase)),
                                            "longest_streak" => guild.taboos.sort_by(|a, b| b.longest_streak.cmp(&a.longest_streak)),
                                            "last_seen" => guild.taboos.sort_by(|a, b| b.last_seen.cmp(&a.last_seen)),
                                            _ => {}
                                        }
                                    }
                                    "reverse" => if let Some(ApplicationCommandInteractionDataOptionValue::Boolean(reverse)) = option.resolved.as_ref() {
                                        if *reverse {
                                            guild.taboos.reverse();
                                        }
                                    }
                                    _ => {}
                                }
                            }

                            match guild.taboos.len() {
                                len if len > 0 => {
                                    let current_time = chrono::offset::Utc::now();

                                    let mut table = "```\n".to_owned();
                                    table.push_str(&format!(
                                        "{:^5} | {:^15} | {:^25} | {:^25}\n",
                                        "Count", "Phrase", "Streak", "Last Seen"
                                    ));
                                    for taboo in guild.taboos {
                                        let mut since = "never".to_owned();
                                        if let Some(last_seen) = taboo.last_seen {
                                            let difference = current_time
                                                .signed_duration_since::<Utc>(last_seen);
                                            let mut f_since = timeago::Formatter::new();
                                            f_since.num_items(2);
                                            f_since.too_low("just now");
                                            since = f_since.convert(difference.to_std().expect("Error converting time::Duration to std::time::Duration. This should not be possible."));
                                        }
                                        let mut streak = "never".to_owned();
                                        if let Some(longest_streak) = taboo.longest_streak {
                                            let mut f_streak = timeago::Formatter::new();
                                            f_streak.num_items(2);
                                            f_streak.ago("");
                                            streak = f_streak.convert(longest_streak.to_std().expect("Error converting time::Duration to std::time::Duration. This should not be possible."))
                                        }
                                        table.push_str(&format!(
                                            "{:<5} | {:15} | {:25} | {:25}\n",
                                            taboo.count, taboo.phrase, streak, since
                                        ));
                                    }
                                    table.push_str("```");

                                    if let Err(why) = interaction.create_interaction_response(&ctx.http, |response| {
                                            response.kind(InteractionResponseType::ChannelMessageWithSource).interaction_response_data(|data| {
                                                data.content(table.to_string())
                                            })
                                        })
                                        .await
                                    {
                                        sentry::capture_error(&why);
                                        error!("Couldn't respond to list command {}", why);
                                    }
                                }
                                _ => {
                                    if let Err(why) = interaction.create_interaction_response(&ctx.http, |response| {
                                            response.kind(InteractionResponseType::ChannelMessageWithSource).interaction_response_data(|data| {
                                                data
                                                    .content("There are currently no taboos in place.")
                                                    .flags(InteractionApplicationCommandCallbackDataFlags::EPHEMERAL)
                                            })
                                        })
                                        .await
                                    {
                                        sentry::capture_error(&why);
                                        error!("Couldn't respond to list command {}", why);
                                    }
                                }
                            }
                        }

                        _ => {}
                    }
                } else if let Err(why) = interaction
                    .create_interaction_response(&ctx.http, |response| {
                        response
                            .kind(InteractionResponseType::ChannelMessageWithSource)
                            .interaction_response_data(|data| {
                                data.content(
                                    "Nothing will ever be taboo between us. Take me to a guild.",
                                )
                            })
                    })
                    .await
                {
                    sentry::capture_error(&why);
                    error!("Couldn't respond to slash command: {}", why);
                }
            }

            Interaction::MessageComponent(interaction) => {
                // Sentry data
                sentry::configure_scope(|scope| {
                    scope.set_user(Some(sentry::User {
                        id: Some(interaction.user.id.to_string()),
                        username: Some(format!(
                            "{}#{}",
                            &interaction.user.name,
                            interaction.user.discriminator.to_string()
                        )),
                        ..Default::default()
                    }));

                    let mut context = BTreeMap::new();
                    context.insert(
                        String::from("guild"),
                        interaction.guild_id.unwrap_or_default().to_string().into(),
                    );
                    context.insert(String::from("kind"), interaction.kind.num().into());
                    scope.set_context(
                        "MessageComponent",
                        sentry::protocol::Context::Other(context),
                    );
                });

                // Throw away any interactions not sent within a guild
                if let Some(guild_id) = interaction.guild_id {
                    let mut ctx_data = ctx.data.write().await;
                    let redis = ctx_data.get_mut::<RedisConnectionManager>().unwrap();

                    let mut guild = Guild::new(redis, guild_id).await;

                    #[allow(clippy::single_match)]
                    match interaction.data.custom_id.as_str() {
                        "delete_taboos" => {
                            for (i, phrase) in interaction.data.values.to_owned().iter().enumerate()
                            {
                                // Rather than checking if the Taboo is still registered
                                // should the Guild struct keep track of the active /delete interaction id
                                // and pass that along in the interaction.data.custom_id?
                                if guild
                                    .taboos
                                    .iter()
                                    .any(|taboo| taboo.phrase == phrase.as_str())
                                {
                                    guild.delete_taboo(redis, phrase).await;
                                    if i == 0 {
                                        if let Err(why) = interaction
                                            .create_interaction_response(&ctx.http, |response| {
                                                response
                                                    .kind(InteractionResponseType::ChannelMessageWithSource)
                                                    .interaction_response_data(|data| {
                                                        data.content(format!(
                                                            "The taboo on \"{}\" has been removed.",
                                                            phrase
                                                        ))
                                                    })
                                            })
                                            .await
                                        {
                                            sentry::capture_error(&why);
                                            error!("Couldn't respond to taboo delete: {}", why);
                                        }
                                    } else if let Err(why) = interaction
                                        .create_followup_message(&ctx.http, |message| {
                                            message.content(format!(
                                                "The taboo on \"{}\" has been removed.",
                                                phrase
                                            ))
                                        })
                                        .await
                                    {
                                        sentry::capture_error(&why);
                                        error!("Couldn't respond to taboo delete: {}", why);
                                    }
                                } else {
                                    #[allow(clippy::collapsible_else_if)]
                                    if i == 0 {
                                        if let Err(why) = interaction
                                            .create_interaction_response(&ctx.http, |response| {
                                                response
                                                .kind(InteractionResponseType::ChannelMessageWithSource)
                                                .interaction_response_data(|data| {
                                                    data.content(format!(
                                                        "There is currently no taboo on \"{}\".",
                                                        phrase
                                                    ))
                                                })
                                            })
                                            .await
                                        {
                                            sentry::capture_error(&why);
                                            error!("Couldn't respond to taboo delete: {}", why);
                                        }
                                    } else if let Err(why) = interaction
                                        .create_followup_message(&ctx.http, |message| {
                                            message.content(format!(
                                                "There is currently no taboo on \"{}\".",
                                                phrase
                                            ))
                                        })
                                        .await
                                    {
                                        sentry::capture_error(&why);
                                        error!("Couldn't respond to taboo delete: {}", why);
                                    }
                                }
                            }
                            // Here would be a great spot to make the Ephemeral response unusable.
                            // However, Discord's API does not support doing basically anything
                            // with an Ephemeral response. Hopefully this changes.
                        }

                        _ => {}
                    }
                }
            }

            _ => {}
        }
    }

    #[tracing::instrument(level = "trace", skip(self, ctx))]
    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("{} is connected!", ready.user.name);

        let commands = ApplicationCommand::set_global_application_commands(&ctx.http, |commands| {
            commands
                // Configuration command
                .create_application_command(|command| {
                    command
                        .name("config")
                        .description("Configure this bot")
                        .create_option(|option| {
                            option
                                .name("administrator")
                                .description("A role permitting taboos to be managed")
                                .kind(ApplicationCommandOptionType::Role)
                                .required(true)
                        })
                })
                // Administrator commands
                .create_application_command(|command| {
                    command
                        .name("add")
                        .description("Create a new taboo in this server")
                        .default_permission(false)
                        .create_option(|option| {
                            option
                                .name("phrase")
                                .description("The word/phrase to taboo")
                                .kind(ApplicationCommandOptionType::String)
                                .required(true)
                        })
                        .create_option(|option| {
                            option
                                .name("alt1")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt2")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt3")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt4")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt5")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt6")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt7")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt8")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                        .create_option(|option| {
                            option
                                .name("alt9")
                                .description(
                                    "A similar word/phrase to taboo (plural, synonym, etc.)",
                                )
                                .kind(ApplicationCommandOptionType::String)
                                .required(false)
                        })
                })
                .create_application_command(|command| {
                    command
                        .name("delete")
                        .description("Remove a taboo in this server")
                        .default_permission(false)
                })
                // General commands
                .create_application_command(|command| {
                    command
                        .name("list")
                        .description("List all taboos in this server")
                        .create_option(|option| {
                            option
                                .name("sort")
                                .description("Sort by")
                                .kind(ApplicationCommandOptionType::String)
                                .add_string_choice("count", "count")
                                .add_string_choice("phrase", "phrase")
                                .add_string_choice("longest streak", "longest_streak")
                                .add_string_choice("last seen", "last_seen")
                        })
                        .create_option(|option| {
                            option
                                .name("reverse")
                                .description("Reverse sorting")
                                .kind(ApplicationCommandOptionType::Boolean)
                        })
                })
        })
        .await;

        info!("Registered commands: {:#?}", commands.unwrap())
    }
}
