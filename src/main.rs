mod guild;
mod handler;
mod taboo;

pub mod redis_connection {
    use serenity::prelude::*;

    pub struct RedisConnectionManager;

    impl TypeMapKey for RedisConnectionManager {
        type Value = redis::aio::ConnectionManager;
    }
}

use std::{env, error::Error};

use dotenv::dotenv;
use serenity::prelude::*;
use tracing::{debug, error};
use tracing_subscriber::prelude::*;

use crate::handler::Handler;
use crate::redis_connection::RedisConnectionManager;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    // Load .env file
    if let Some(value) = env::var_os("USE_DOTENV") {
        if value == *"true" {
            dotenv().ok();
        }
    }

    // Logging config
    env_logger::Builder::from_env(
        env_logger::Env::new()
            .filter_or("LOG_LEVEL", "info")
            .write_style_or("LOG_STYLE", "always"),
    )
    .filter_module("tracing::span", log::LevelFilter::Warn)
    .filter_module("serenity", log::LevelFilter::Warn)
    .init();

    // Tracing config
    let tracer = opentelemetry_jaeger::new_pipeline()
        .with_service_name("TabooBot")
        .install_batch(opentelemetry::runtime::Tokio)?;
    let telemetry = tracing_opentelemetry::layer().with_tracer(tracer);
    let subscriber = tracing_subscriber::Registry::default().with(telemetry);
    tracing::subscriber::set_global_default(subscriber)
        .expect("Expected to set global tracing configuration");

    // Sentry config
    let _guard = sentry::init((
        env::var_os("SENTRY_DSN").unwrap_or_default(),
        sentry::ClientOptions {
            release: sentry::release_name!(),
            debug: true,
            ..Default::default()
        },
    ));

    // Redis config
    // redis://[<username>][:<password>@]<hostname>[:port][/<db>]
    debug!("Redis connection variables");
    let mut redis_url = "redis://".to_owned();
    if let Some(username) = env::var_os("REDIS_USER") {
        if let Some(username) = username.to_str() {
            debug!(?username);
            if !username.is_empty() {
                redis_url.push_str(username);
                // redis_url.push_str("@"); // if no password??
            }
        }
    }
    if let Some(password) = env::var_os("REDIS_PASSWORD") {
        if let Some(password) = password.to_str() {
            debug!(?password);
            if !password.is_empty() {
                redis_url.push(':');
                redis_url.push_str(password);
                redis_url.push('@');
            }
        }
    }
    match env::var_os("REDIS_HOST") {
        Some(host) => {
            if let Some(host) = host.to_str() {
                debug!(?host);
                if !host.is_empty() {
                    redis_url.push_str(host);
                }
            }
        }
        None => {
            redis_url.push_str("127.0.0.1");
        }
    }
    redis_url.push(':');
    match env::var_os("REDIS_PORT") {
        Some(port) => {
            if let Some(port) = port.to_str() {
                debug!(?port);
                if !port.is_empty() {
                    redis_url.push_str(port);
                } else {
                    redis_url.push_str("6379");
                }
            }
        }
        None => {
            redis_url.push_str("6379");
        }
    }
    if let Some(db) = env::var_os("REDIS_DB") {
        if let Some(db) = db.to_str() {
            debug!(?db);
            if !db.is_empty() {
                redis_url.push('/');
                redis_url.push_str(db);
            }
        }
    }

    debug!(?redis_url);

    let redis_client = redis::Client::open(redis_url).unwrap();
    let redis_con = redis_client
        .get_tokio_connection_manager()
        .await
        .expect("Failed to get a Redis connection manager");

    // Discord config
    let token = env::var("BOT_TOKEN").expect("Expected a token in the environment");
    let application_id: u64 = env::var("APPLICATION_ID")
        .expect("Expected an application id in the environment")
        .parse()
        .expect("application id is not a valid id");

    let mut client = Client::builder(token)
        .event_handler(Handler)
        .application_id(application_id)
        .await
        .expect("Error creating client");
    {
        let mut client_data = client.data.write().await;
        client_data.insert::<RedisConnectionManager>(redis_con);
    }

    if let Err(why) = client.start_autosharded().await {
        error!("Client error: {:?}", why);
    }

    Ok(())
}
