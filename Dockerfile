FROM rust:1.59.0@sha256:12993d6e83049487cabb7afe0864535140f6e8b0322a45532a0a337fac870355 as builder
WORKDIR /taboobot
COPY ./Cargo.toml Cargo.toml
COPY ./Cargo.lock Cargo.lock
# Building with a dummy main.rs allows Docker
# to cache compiled build dependencies
RUN mkdir ./src && echo 'fn main() { println!("Dummy!"); }' > ./src/main.rs
RUN cargo build --release --locked
RUN rm -rf ./src
# Now bring in the actual source code to build
COPY ./src ./src
RUN touch -a -m ./src/main.rs
RUN cargo build --release --locked


FROM debian:buster-slim@sha256:e61cffb42ef0dbb31832d4543d6998ab9125210d0136bc0aed65cc753cee9125

# Build arguments
ARG S6_OVERLAY_VERSION="v2.2.0.3"

# Install s6 Overlay process manager (https://github.com/just-containers/s6-overlay)
ADD https://github.com/just-containers/s6-overlay/releases/download/${S6_OVERLAY_VERSION}/s6-overlay-amd64-installer /tmp/
RUN chmod +x /tmp/s6-overlay-amd64-installer && /tmp/s6-overlay-amd64-installer / && rm /tmp/s6-overlay-amd64-installer

RUN apt-get update && apt-get install -y \
  ca-certificates \
  libssl-dev \
  && rm -rf /var/lib/apt/lists/*

# Install taboobot
COPY --from=builder /taboobot/target/release/taboobot /usr/local/bin/taboobot

# TabooBot Defaults
ENV USE_DOTENV="false"
ENV LOG_LEVEL=debug
ENV LOG_STYLE=always
ENV SENTRY_DSN=
ENV REDIS_USER=
ENV REDIS_PASSWORD=
ENV REDIS_HOST=127.0.0.1
ENV REDIS_PORT=6379
ENV REDIS_DB=
ENV BOT_TOKEN=
ENV APPLICATION_ID=

# S6 Configuration
ENV S6_READ_ONLY_ROOT=1

# Drop root
RUN addgroup taboobot && adduser --ingroup taboobot taboobot
USER taboobot

ENTRYPOINT ["/init"]
CMD ["taboobot"]
