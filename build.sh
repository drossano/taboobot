echo "Adding targets..."
# rustup target add x86_64-apple-darwin
rustup target add x86_64-unknown-linux-gnu
# rustup target add x86_64-pc-windows-msvc
# rustup target add aarch64-apple-darwin
# rustup target add aarch64-unknown-linux-gnu
# rustup target add aarch64-pc-windows-msvc
echo "Finished adding targets."

echo "Building..."
# echo "x86_64-apple-darwin"
# cargo build --locked --release --target=x86_64-apple-darwin && mkdir -p dist/taboobot_darwin_amd64 && cp target/x86_64-apple-darwin/release/taboobot dist/taboobot_darwin_amd64
echo "x86_64-unknown-linux-gnu"
cargo build --locked --release --target=x86_64-unknown-linux-gnu && mkdir -p dist/taboobot_linux_amd64 && cp target/x86_64-unknown-linux-gnu/release/taboobot dist/taboobot_linux_amd64
# echo "x86_64-pc-windows-msvc"
# cargo build --locked --release --target=x86_64-pc-windows-msvc && mkdir -p dist/taboobot_windows_amd64 && cp target/x86_64-pc-windows-msvc/release/taboobot.exe dist/taboobot_windows_amd64

# echo "aarch64-apple-darwin"
# cargo build --locked --release --target=aarch64-apple-darwin && mkdir -p dist/taboobot_darwin_arm64 && cp target/aarch64-apple-darwin/release/taboobot dist/taboobot_darwin_arm64
# echo "aarch64-unknown-linux-gnu"
# cargo build --locked --release --target=aarch64-unknown-linux-gnu && mkdir -p dist/taboobot_linux_arm64 && cp target/aarch64-unknown-linux-gnu/release/taboobot dist/taboobot_linux_arm64
# echo "aarch64-pc-windows-msvc"
# cargo build --locked --release --target=aarch64-pc-windows-msvc && mkdir -p dist/taboobot_windows_arm64 && cp target/aarch64-pc-windows-msvc/release/taboobot.exe dist/taboobot_windows_arm64
echo "Finished building."
